   // ./game.js
// serverside game logic
/**
 * @file Serverside game logic
 * @copyright ISC
*/
'use strict';
const Promise = require('bluebird');
const fs = Promise.promisifyAll(require('fs'));
const path = require('path');
const _ = require('lodash');
const dictDir = require('./config').dictDir || 'data/dicts';
const defaultDict = [
  'dog',
  'cat',
  'sun',
  'book',
  'ice_cream'
]

/**
 * Generates a random int between min(inclusive) and max(exclusive)
 * @param {number} min
 * @param {number} max
 * @returns {number} An int between min(inclusive) and max(exclusive)
*/
function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

function genGameId() {
  let alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
  let id = '';
  for (let i = 0; i < 4; i++) {
    id += alphabet[getRandomInt(0, 25)];
  }
  return id;
}

function normalizeDict(dict) {
  _.forEach(dict, (entry, index, array) => {
    entry = entry.replace(/\s/, '_');
    entry = entry.replace(/[^\w]/, '');
  });
  _.remove(dict, (entry) => {
    return /^[w]{1,30}$/.test(entry);
  });
  if (dict.length === 0) {
    throw new Error('All dictionary entries invalid')
  }
  return dict;
}

/**
 * Loads dictionaries from file.
 * @param {string} fPath path to dictionary file
 * @returns {array} Dictionary, string array
*/
async function loadDict(fName) {
  /* File should be in /data/dicts
   * Dictionary file should be either JSON like:
   * {
   *   [
   *     'entry1',
   *     'entry2'
   *   ]
   * }
   * or newline seperated entries like:
   * entry1
   * entry2
  */
  let fPath = path.join(__dirname, dictDir, fName);
  var dict = await fs.readFileAsync(fPath, 'utf8').then((contents) => {
    if (/\.json$/.test(fPath)) {
      try {
        let dict = JSON.parse(contents);
        if (dict instanceof Array) {
          return dict;
        } else if (dict.dict instanceof Array) {
          // maybe add config options later
          return dict.dict;
        } else {
          throw new Error('invalid dictionary json');
        }
      } catch(err) {
        console.error(err);
        throw err;
      }
    }
    let lines = contents.split('\n');
    let dict = [];
    _.forEach(lines, (line) => {

      dict.push(line);
    });
    console.log(dict);
    return dict;
  });
  return normalizeDict(dict);
}

async function loadDicts(dicts) {
  if (typeof dicts === 'string') {
    return loadDict(dicts);
  } else if
}

/**
 * Returns a new Game
 * @class
 * @classdesc Game is an implementation of picture-game logic ie this is a game
 * @param {Array} dict an array of strings, possible words to be chosen
*/
function Game(dict) {
  // TODO: Event Emitter
  if (typeof new.target === 'undefined') {
    throw new SyntaxError('You must use the new syntax to call game');
  }
  if (typeof dict !== 'object' || !dict instanceof Array) dict = defaultDict;
  this.users = [];
  var word = '';
  var wordHistory = [];
  var round = {};
  var rounds = [];
  var state = 'start';
  // getters
  Object.defineProperties(this, {
    'userCount': {
      get() {
        return this.users.length;
      }
    },
    'word': {
      get() {
        return word;
      }
    },
    'state': {
      get() {
        return state;
      }
    },
    'dict': {
      get() {
        return dict;
      },
      set(newDict) {
        let normResult = normalizeDict(newDict);
        if (normResult instanceof Array && normResult.length > 0) {
          dict = normResult;
        }
      }
    },
    'id': {
      value: genGameId()
    },
    'scoreboard': {
      get() {
        return _.sortBy(this.users, ['score', 'nick'])
      }
    },
    'correctUsers': {
      get() {
        return round.winners.length;
      }
    }
  });
  this.changeWord = function changeWord(recurse) {
    // if recurse is true this function is called by itself
    if (!recurse && word !== '') wordHistory.push(word);
    console.log(dict);
    console.log(wordHistory);
    if (dict.length === wordHistory.length) {
      // all words in history have been used
      return false;
    }
    let index = getRandomInt(0, this.dict.length);
    let newWord = this.dict[index];
    if (_.includes(wordHistory, newWord)) {
      return this.changeWord(true);
    } else {
      word = newWord;
      return true;
    }
  }
  this.join = function join(userId, nick) {
    let user = {
      id: userId,
      nick: nick || '',
      points: 0,
    }
    if (state === 'start') user.role = Game.ROLE_NONE;
    if (state === 'round') user.role = Game.ROLE_SPECTATE;
    if (state === 'change') user.role = Game.ROLE_GUESS;
    if (state === 'end') user.role = Game.ROLE_SPECTATE;
    this.users.push(user);
    return user;
  }

  this.start = function start() {
    console.log('game starting')
    state = 'change';
    this.users[0].role = Game.ROLE_DRAW;
    for (let i = 1; i < this.users.length; i++) {
      this.users[i].role = Game.ROLE_GUESS;
    }
    round = {
      id: rounds.length, // it's index in the rounds array
      artistId: 0, // this is because we know it's the first user
      winners: [],
      users: this.users
    }
    var done = !this.changeWord(); // returns false if we've used all the words
    if (done) {
      // this.end();
      return false;
    }
    state = 'round';
    return true;
  }

  this.guess = function guess(user, guess) {
    if (this.state !== 'round') return;
    guess = guess.replace(/\s/g, '_'); // replaces any spaces in word with _
    guess = guess.toLowerCase();
    if (guess === this.word) {
      if (round.winners.length === 0) {
        user.points += 2;
      } else {
        user.points += 1;
      }
      round.winners.push(user.id);
      return true;
    } else {
      return false;
    }
  }

  this.nextRound = function nextRound(pic) {
    console.log('round changing');
    state = 'change';
    round.users = _.cloneDeep(this.users); // archive users
    if (typeof pic !== 'undefined') round.pic = pic;
    rounds.push(round); // save game state of previous round
    let newArtist = round.artistId + 1;
    if (newArtist >= this.users.length) newArtist = 0;
    console.log('new artist id: ' + newArtist);
    for(let i = 0; i < this.users.length; i++) {
      let user = this.users[i];
      if (user.role === Game.ROLE_SPECTATE) user.role = Game.ROLE_GUESS;
      if (user.role === Game.ROLE_DRAW) user.role = Game.ROLE_GUESS;
      if (i === newArtist) user.role = Game.ROLE_DRAW;
    }
    round = {
      id: rounds.length,
      artistId: parseInt(_.findKey(this.users, {'role': Game.ROLE_DRAW})),
      winners: [], // id's of users who guessed correctly
      users: this.users
    }
    let done = !this.changeWord(); // returns false if all words have been used
    if (done) {
      // this.end();
      return false;
    }
    state = 'round';
    return true;
  }

  this.end = function end() {
    console.log('game ending');
    state = 'change';
    this.winner = this.scoreboard[0];
    state = 'end';
  }

  this.getUser = function getUser(id) {
    return _.find(this.users, ['id', id]);
  }
}

// constants
// Game.CONSTANT = value;
Game.ROLE_NONE = 0;
Game.ROLE_GUESS = 1;
Game.ROLE_DRAW = 2;
Game.ROLE_SPECTATE = 3;

// static methods
// Game.staticMethod = function() {}


module.exports = {
  Game: Game,
  loadDict: loadDict
}
