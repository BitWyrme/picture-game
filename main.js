#!/usr/bin/env node
'use strict';
const http = require('http');
const fs = require('fs');
const path = require('path');
const url = require('url');
const Primus = require('primus');
const _ = require('lodash');
const express = require('express');
const app = express();
const dict = require('./dict.js');

const config = require('./config') || {};
config.log = config.log || false;
config.port = config.port || 3000;

app.use('/', (req, res, next) => {
  if (config.log) {
    let ip;
    if (/\.*/.test(req.ip)) {
      // extracts IPv4 from IPv6 syntax
      ip = /[\d.]+/.exec(req.ip)[0];
    } else {
      ip = req.ip;
    }
    console.log(`${ip} made a ${req.method} request for ${req.originalUrl}`);
  }
  next();
});

app.use(express.static('public'));

const server = app.listen(config.port);

console.log(`Listening on port ${config.port}`);

const primus = new Primus(server, {
  transformer: 'websockets', // ws module
  parser: 'binary'
});

/**
 * Generator which produces incremental numbers starting at start or 1
 * @generator
 * @function incGen
 * @param {number} start Starting point, 1 by default
 * @yields {number} The next id number
*/
function* incGen(start) {
  let id = start || 1;
  while(true) {
    yield id;
    id += 1;
  }
}

function next(gen) {
  return gen.next().value;
}

const {Game, loadDict} = require('./game.js');

var games = [];

let idGen = incGen();

primus.on('connection', (spark) => {
  spark.uid = next(idGen);
  // spark.game = defaultGame;
  spark.on('data', (data) => {
    if (!'action' in data || data.action !== 'sendPic') {
      console.log(data);
    }
    if (typeof data === 'object') {
      processData(spark, data);
    }
  });
});

function writeToGame(game, data) {
  // game is game to match to
  // data is data to transmit
  console.log('Writing to game');
  console.log(game);
  console.log(data);
  primus.forEach((spark, next) => {
    if (spark.game.id === game.id) {
      spark.write(data);
    }
    next();
  }, (err) => {
    // called when done
    if (typeof err !== 'undefined') console.error(err);
  });
}

function processData(spark, data) {
  if (!'action' in data) return;
  switch (data.action) {
    case 'sendPic':
      recievePic(spark, data.pic);
      break;
    case 'getScoreboard':
      spark.write({
        action: 'sendScoreboard',
        scoreboard: spark.game.scoreboard
      });
      break;
    case 'createGame':
      createGame(spark, data);
      break;
    case 'joinGame':
      joinGame(spark, data);
      break;
    case 'guess':
      guessWord(spark, data.word)
      break;
  }
}

function createGame(spark, data) {
  console.log('user creating game');
  var game;
  if ('codes' in data) {
    // get dictionaries by codes
    // then append them together
    let codeArr = data.codes.split(',');
    let dicts = []; // two dimensional array of dictionaries
    _.forEach(codeArr, (code) => {
      let dict;
      try {
        dict = loadDict(code);
        dicts.push(dict);
      } catch(err) {
        console.log('invalid code');
        spark.write({
          action: 'error',
          message: 'invalid dictionary code'
        });
      }
    });
    let flatDict = _.flattenDeep(dicts);
    game = new Game(flatDict);
  } else {
    game = new Game();
  }
  game.sparks = [];
  game.sparks.push(spark.id);
  games.push(game);
  spark.game = game;
  spark.user = game.join(spark.uid, data.nick);
  spark.write({
    action: 'join',
    code: spark.game.id
  });
}

function joinGame(spark, data) {
  console.log('user joining game...');
  data.code = data.code.toUpperCase();
  var game = _.find(games, ['id', data.code]);
  if (typeof game === 'undefined') {
    spark.write({
      action: 'error',
      type: 'join',
      message: 'Invalid game code'
    });
    return false;
  }
  spark.game = game;
  game.join(spark.uid, data.nick);
  spark.write({
    action: 'join'
  });
  if (spark.game.users.length >= 2) {
    startGame(spark.game);
  }
}

function guessWord(spark, word) {
  if (!'game' in spark) return false;
  var user = spark.game.getUser(spark.uid);
  var result = spark.game.guess(user, word);
  if (result) {
    spark.write({
      action: 'guess',
      result: true,
      points: user.points
    });
    if (spark.game.correctUsers >= spark.game.users.length - 1) {
      nextRound(spark.game);
    }
  } else {
    spark.write({
      action: 'guess',
      result: false
    });
  }
}

function startGame(game) {
  game.start();
  startRound(game);
}

function nextRound(game) {
  let proceed = game.nextRound(); // can provide current picture for archiving
  if (!proceed) {
    endGame(game);
  } else {
    startRound(game);
  }
}

function startRound(game) { // internal function called by startGame and nextRound
  primus.forEach((spark, next) => {
    if ('game' in spark && spark.game.id === game.id) {
      let user = spark.game.getUser(spark.uid);
      console.log(user);
      if (user.role === Game.ROLE_DRAW) {
        spark.write({
          action: 'start',
          role: Game.ROLE_DRAW,
          word: spark.game.word.replace(/_/g, ' ') // replace _ with spaces
        });
      } else {
        spark.write({
          action: 'start',
          role: user.role
        });
      }
    }
    next();
  }, (err) => {
    if (typeof err !== 'undefined') console.error(err);
  });
}

function endGame(game) {
  game.end();
  writeToGame(game, {
    action: 'end',
    winner: game.winner,
    scoreboard: game.scoreboard
  });
}

function recievePic(sendSpark, pic) {
  if (!'game' in sendSpark) {
    primus.forEach((spark, next) => {
      if (!'game' in spark) {
        spark.write({
          action: 'sendPic',
          pic: pic
        });
      }
      next();
    }, (err) => {
      // runs on complete or err
      if (typeof err !== 'undefined') console.error(err);
    });
  } else {
    primus.forEach((spark, next) => {
      if ('game' in spark) {
        let user = spark.game.getUser(spark.uid);
        if (user.role !== Game.ROLE_DRAW) {
          spark.write({
            action: 'sendPic',
            pic: pic
          });
        }
      }
      next();
    }, (err) => {
      // runs on complete or error
      if (typeof err !== 'undefined') console.error(err);
    });
  }
}
