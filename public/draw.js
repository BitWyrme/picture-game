// TODO: add undo/redo buttons:
// call ctx.save on mousedown, then ctx.restore on undo button
'use strict';
function getCursorPosition(canvas, event) {
  var rect = canvas.getBoundingClientRect();
  var x = event.clientX - rect.left;
  var y = event.clientY - rect.top;
  return {x: x, y: y};
}
self.color = {
  pickerColor: 'black',
  customColor: ''
};
(() => {
  const colorPicker = document.getElementById('colorPicker');
  const drawBox = document.getElementById('drawBox');
  const config = {
    colors: [
      'white',
      'black',
      'red',
      'orange',
      'yellow',
      'blue',
      'indigo',
      'violet'
    ]
  }
  while (config.colors.length < 10) {
    config.colors.push('white');
  }
  // Color picker setup
  function colorMouseHandler(event) {
    let {x, y} = getCursorPosition(colorPicker, event);
    let index = Math.floor(x/60);
    let color = config.colors[index];
    // change background of draw box to show current color;
    if (self.color.customColor.length === 0) {
      drawBox.style.backgroundColor = color;
    }
    // export color to global object w/ self keyword
    self.color.pickerColor = color;
  }

  function setupColorPicker() {
    // get 2d context
    let ctx = colorPicker.getContext('2d');
    // colors is filled with CSS color strings, to populate the picker
    // currently it is stored in config object
    for (let i = 0; i < 600; i += 60) {
      let colorsIndex = i / 60;
      ctx.fillStyle = config.colors[colorsIndex];
      /* // debugging stuff
      console.log(colorsIndex);
      console.log(config.colors[colorsIndex]);
      console.log('Coords: ' + i +', 0, 60, 60');
      console.log('Style: ' + ctx.fillStyle);
      */
      ctx.fillRect(i, 0, 60, 60);
      // ctx.addHitRegion not standardized
    }
    // do not keep adding event listeners on second try
    colorPicker.removeEventListener('click', colorMouseHandler);
    colorPicker.addEventListener('click', colorMouseHandler);
  }
  setupColorPicker();
  self.picker = colorPicker;
})();

function registerCanvasMove(func) {
  // each of functions in array are called when canvas is written to by program
  registerCanvasMove.listeners = registerCanvasMove.listeners || [];
  if (typeof func === 'function') {
    registerCanvasMove.listeners.push(func);
  }
}

(() => {
  // draw screen
  const drawScreen = document.getElementById('drawScreen');
  self.draw = drawScreen;
  let mouseDown = false;
  var px, py;
  function canvasWrite() {
    _.forEach(registerCanvasMove.listeners, (listener) => {
      listener(drawScreen);
    });
  }
  document.addEventListener('mouseup', (event) => {
    mouseDown = false;
  });
  drawScreen.addEventListener('mousedown', (event) => {
    mouseDown = true;
    if (typeof px === 'number' && typeof py === 'number') {
      let {x, y} = getCursorPosition(drawScreen, event);
      px = x;
      py = y;
    }
  });
  let ctx = drawScreen.getContext('2d');
  drawScreen.addEventListener('mousemove', (event) => {
    if (!mouseDown) return;
    let {x, y} = getCursorPosition(drawScreen, event);
    let penColor = '';
    if (self.color.customColor.length > 0) {
      penColor = self.color.customColor;
    } else if (self.color.pickerColor.length > 0) {
      penColor = self.color.pickerColor;
    } else {
      console.log('fallback color, shouldn\'t happen');
      penColor = 'black';
    }
    ctx.fillStyle = penColor;
    ctx.strokeStyle = penColor;
    if (typeof px === 'number' && typeof py === 'number') {
      ctx.beginPath();
      ctx.moveTo(px, py);
      ctx.lineTo(x, y);
      ctx.stroke();
    } else {
      ctx.fillRect(x, y, 1, 1);
    }
    px = x;
    py = y;
    canvasWrite();
  });
  function resetDraw() {
    ctx.clearRect(0, 0, drawScreen.width, drawScreen.height);
    ctx.beginPath();
    canvasWrite();
  }
  self.resetDraw = resetDraw;
  document.getElementById('reset-canvas').addEventListener('click', resetDraw);
})();
