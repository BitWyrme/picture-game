(() => {
  const modal = document.getElementById('modal');
  const modalTitle = document.getElementById('modal-title');
  const modalMessage = document.getElementById('modal-message');
  const modalButtons = document.getElementById('modal-buttons');
  const modalConfirm = document.getElementById('modal-confirm');
  const modalCancel = document.getElementById('modal-cancel');
  function showModal() {
    modal.classList.remove('hidden');
  }
  function hideModal() {
    modal.classList.add('hidden');
  }
  var Modal = {
    message(title, message, cb) {
      if (typeof title === 'string') {
        modalTitle.classList.remove('hidden');
        modalTitle.innerText = title;
      } else {
        modalTitle.classList.add('hidden');
      }
      modalMessage.innerText = message;
      var listener;
      if (typeof cb === 'function') {
        listener = (evt) => {
          hideModal();
          modalConfirm.removeEventListener('click', listener);
          cb();
        }
      } else {
        listener = (evt) => {
          hideModal();
          modalConfirm.removeEventListener('click', listener);
        }
      }
      modalConfirm.addEventListener('click', listener, false);
      showModal();
    },
    show: showModal,
    hide: hideModal
  }
  self.Modal = Modal;
})();
