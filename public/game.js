'use strict';
var appPages = document.getElementsByClassName('appPage');
self.pic = document.getElementById('pic-stream');
self.activeScreen = 'start';
const roles = { // constants
  ROLE_NONE: 0,
  ROLE_GUESS: 1,
  ROLE_DRAW :  2,
  ROLE_SPECTATE: 3
}
self.gameRole = roles.ROLE_NONE;
function changeActive(element) {
  // element must be an element in appPages
  // or element id
  if (typeof element === 'string') {
    let arr = _.filter(appPages, {id: element});
    element = arr[0];
  }
  if(_.indexOf(appPages, element) === -1){
    return false;
  } else {
    _.forEach(appPages, (ele) => {
      ele.classList.add('hidden');
    });
    element.classList.remove('hidden');
    switch (element.id) {
      case 'startPage':
        self.activeScreen = 'start';
        break;
      case 'drawBox':
        self.activeScreen = 'draw';
        break;
      case 'guessBox':
        self.activeScreen = 'guess';
        break;
      case 'loadingScreen':
        self.activeScreen = 'loading';
        break;
    }
  }
}

document.querySelectorAll('.menu-trigger').forEach((trigger) => {
  let menu = document.getElementById(trigger.dataset.menu);
  let parent = trigger.parentElement;
  let parentQuery = '';
  if (parent.id.length > 0) {
    parentQuery += '#' + parent.id;
  } else {
    parentQuery += '.menu-container';
  }

  trigger.addEventListener('click', (event) => {
    toggleMenu(menu);
  });
  document.addEventListener('click', (event) => {
    try {
      if (!event.target.closest(parentQuery)) {
        hideMenu(menu);
      }
    } catch(err) {
      console.log(event);
    }
  });
});

function showMenu(menu) {
  if (typeof menu === 'string') menu = document.getElementById(menu);
  menu.classList.remove('hidden');
  //document.getElementById('display-menu-box').classList.remove('hidden');
}

function hideMenu(menu) {
  if (typeof menu === 'string') menu = document.getElementById(menu);
  menu.classList.add('hidden');
  //document.getElementById('display-menu-box').classList.add('hidden');
}

function toggleMenu(menu) {
  if (typeof menu === 'string') menu = document.getElementById(menu);
  menu.classList.toggle('hidden');
}

(() => {
  document.querySelector('#show-start').addEventListener('click', (event) => {
    changeActive('startPage');
    document.location.hash = 'nav-man-start';
  });
  document.querySelector('#show-draw').addEventListener('click', (event) => {
    changeActive('drawBox');
    document.location.hash = 'nav-man-draw';
  });
  document.querySelector('#show-guess').addEventListener('click', (event) => {
    changeActive('guessBox');
    document.location.hash = 'nav-man-guess';
  });
  document.querySelector('#advanced-color-pick').addEventListener('click', (event) => {
    let customColor = window.prompt('Enter a CSS color selector. \nLeave blank to default to color picker');
    if (customColor === null) customColor = '';
    if (customColor.length >= 1) {
      document.getElementById('drawBox').style.backgroundColor = customColor;
    }
    self.color.customColor = customColor;
  });
  document.querySelector('#line-width-pick').addEventListener('click', (event) => {
    let lineWidth = window.prompt('Pick Line width');
    self.draw.getContext('2d').lineWidth = lineWidth;
  });
  function hashPageChange() {
    // checks document.location.hash
    // and changes page if it matches a page navigator
    console.log('Checking hash...');
    let navPage = /#nav-(?:man-)?(.*)/.exec(document.location.hash);
    if (navPage instanceof Array) {
      navPage = navPage[1];
      switch (navPage) {
        case 'start':
          changeActive('startPage');
          break;
        case 'draw':
          changeActive('drawBox');
          break;
        case 'guess':
          changeActive('guessBox');
          break;
      }
    } else {
      console.log('Invalid hash');
    }
  }
  hashPageChange();
  document.addEventListener('load', hashPageChange);
})();

function localDrawToPic(loop, delay) {
  // test function
  draw.toBlob((blob) => {
    pic.src = URL.createObjectURL(blob);
  });
  loop = loop || false;
  if (loop) {
    delay = delay || 100;
    window.setTimeout(localDrawToPic, delay, loop);
  }
}

const primusURL = window.location.protocol + '//' + window.location.host;
const primus = new Primus(primusURL);
var prevPicURL = '';
(() => {
  // picture stream/primus
  function recievePic(newPic) {
    if (self.activeScreen === 'guess') {
      let blob = new Blob([newPic], {type: 'image/png'});
      let url = URL.createObjectURL(blob);
      pic.src = url;
      URL.revokeObjectURL(prevPicURL);
      prevPicURL = url;
    }
  }
  primus.on('data', (data) => {
    if (!'action' in data) {
      console.log(data);
      return;
    }
    console.log(data);
    switch (data.action) {
      case 'sendPic':
        recievePic(data.pic);
        break;
      case 'sendScoreboard':
        console.log(data.scoreboard);
        break;
      case 'join':
        if ('game' in data) {
          console.log(data.game);
          if ('dir' in console) console.dir(data.game);
          if ('table' in console) console.table(data.game);
        }
        console.log('joined game');
        changeActive('loadingScreen');
        if ('code' in data) {
          console.log('Join code: ' + data.code);
          self.gameJoinCode = data.code;
          alert('Join code: ' + data.code);
        }
        break;
      case 'guess':
        let guessBox = document.getElementById('guessBox');
        if (data.result) {
          document.getElementById('guess-input').value = '';
          alert('Correct guess!\nYou now have ' + data.points + ' points.');
          guessBox.classList.add('correct');
          setTimeout(() => {
            guessBox.classList.remove('correct');
          }, 1000);
        } else {
          alert('Incorrect Guess');
          guessBox.classList.add('wrong');
          setTimeout(() => {
            guessBox.classList.remove('wrong');
          }, 1000);
        }
        break;
      case 'error':
        if (data.type === 'join') {
          changeActive('startPage');
          console.error(data.message);
          alert(data.message);
        } else if ('message' in data) {
          console.error(data.message);
          alert(data.message);
        } else {
          let message = 'Unknown Error';
          console.error(message);
          alert(message);
        }
        break;
      case 'start':
        resetDraw();
        self.gameRole = data.role || roles.ROLE_GUESS;
        if (gameRole === roles.ROLE_DRAW) {
          changeActive('drawBox');
          document.getElementById('drawBox').dataset.word = data.word;
        } else if (gameRole === roles.ROLE_GUESS) {
          changeActive('guessBox');
        } else {
          console.log('role error');
        }
        break;
      case 'end':
        console.log('Game over: ');
        console.log(data.winner.nick + ' won!');
        alert('Game over, ' + data.winner.nick + ' won!');
        setTimeout(() => {
          location.hash = '';
          location.reload(true); // reloads the page from the server
        }, 2000);
        break;
    }
  });

  registerCanvasMove((canvas) => {
    if (gameRole !== roles.ROLE_DRAW) return; // don't do this if not drawing
    canvas.toBlob((blob) => {
      primus.write({
        action: 'sendPic',
        pic: blob
      });
    });
  });
})();

function getScoreBoard() {
  primus.write({
    action: 'getScoreboard'
  });
}

(() => {
  // Create/Join functions
  function getNick() {
    // returns users chosen nickname
    var nick = prompt('Select a nickname 1-30 alphanumeric characters: ');
    if (typeof nick !== 'string' || !/^[\w\s]{1,30}$/.test(nick)) {
      nick = 'Noname chose an invalid name';
    }
    return nick;
  }

  function createGame() {
    let nick = getNick();
    console.log('Nickname: ' + nick);
    let dictCodes = document.getElementById('dictCodes').value;
    if (dictCodes.length === 0) {
      primus.write({
        action: 'createGame',
        nick: nick
      });
    } else {
      primus.write({
        action: 'createGame',
        nick: nick,
        codes: dictCodes
      });
    }
  }

  function joinGame() {
    var code = document.getElementById('joinCode').value;
    let nick = getNick();
    primus.write({
      action: 'joinGame',
      code: code,
      nick: nick
    });
  }

  // add event listeners for createGame and joinGame
  document.getElementById('createSubmit').addEventListener('click', createGame);
  document.getElementById('joinSubmit').addEventListener('click', joinGame);

  function guessWord(word) {
    if (self.gameRole !== roles.ROLE_GUESS) return;
    if (typeof word !== 'string' || word.length < 1) return;
    primus.write({
      action: 'guess',
      word: word
    });
  }

  // add event listener for enter on guess input
  let guessInput = document.getElementById('guess-input');
  guessInput.addEventListener('keydown', (evt) => {
    if (evt.keyCode === 13 || evt.key === 'Enter' || evt.key === 'Return') {
      guessWord(guessInput.value);
    }
  });
})();
